import json
import requests as rq

nm_url = "https://we6.talentsprint.com/wordle/game/"
register_url = nm_url + "register"
create_url = nm_url + "create"
guess_url = nm_url + "guess"
id = ''
if id == '':
    sesh = rq.Session()
    register_dict = {"mode": "Mastermind", "name": "Ishita"}
    reg = sesh.post(register_url, json=register_dict)
    id = reg.json()["id"]
    header = {"Content-Type": "application/json"}
    cookies = reg.cookies.get_dict()

if id:
    creat_dict = {"id": id, "overwrite": True}
    creat = sesh.post(create_url, json=creat_dict)
    print(creat.text)

with open('5letters.txt') as file:
    words = [line.strip() for line in file]

for word in words:
    guess = {"id": id, "guess": word}
    correct = sesh.post(guess_url, json=guess)
    print(correct.text)
    
    feedback = int(correct.text.split(',')[0][-1])
    
    if feedback == 5:
        print(word)
        break
    else:
        filtered_words = []
        for _ in words:
              no_of_matches = sum(int(word[i] == _[i]) for i in range(5))
              if no_of_matches == feedback:
                filtered_words.append(_)
        words = filtered_words
