import random

def readfile():
    wordfile = []
    with open('5letters.txt', 'r') as file:
        for word in file:
            wordfile.append(word.strip())
    return wordfile

def choose_random():
    return random.choice(readfile())

print(choose_random())

def onecorrect(guessed: str):
    extracted_list = []
    for checkchar in guessed:
        exclude = [char for char in guessed if char != checkchar]
        substring = checkchar
        for element in readfile():
            if substring in element and not any(char in element for char in exclude):
                extracted_list.append(element)
    return extracted_list

print(onecorrect("abcd"))



